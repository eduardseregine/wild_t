# About

Frontend part of the "Wir learn Deutsch by texten" application. 

The application provides for:  
 - Interface in English, German or Estonian
 - User's authentication by Google credentials
 - Selecting 2 study languages (German or Estonian), and 2 levels (A2 and B1)
 - Input text field with posibility to introduce special symbols of German and Estonian languages
  - Display result and overall results pages


# Technology

Frontend is a React JS applicationm with the following specifics:
 - interface is responsive for different desctop screen sizes   
 - does not support mobile browsing
 - requires backend to be running simultaniosly (`https://gitlab.com/eduardseregine/wild_be`)
 - currently designed for cloud Firebase deployment, to deploy locally requires changes to GET and POST services to listen to localhost and respective port (presumably 8080)



# Intallation

Clone down this repository. 
You will need to be installed on your machine:

 - node (go to `https://nodejs.org/en/download/`)
 - install npm:
 
 ```
 npm install
 ```

 - check the isntallation by typing:

 ```
 node -v
 ```
 Also, the npm:

```
npm -v
```

- to start, ensure you are in the main directory of the application, and run:

```
npm start
```

- your app will be locally available at:

`localhost:3000/`

