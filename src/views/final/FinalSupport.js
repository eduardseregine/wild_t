import React, {Component} from 'react';
import '../../css/final/final_support.css';
export default class FinalSupport extends Component {
    render() {
        let flags = ['invisible','invisible'];
        flags = this.props.flag !== undefined ? this.props.flag === "DE" ? ['','invisible'] : ['invisible',''] : flags;
        return (
           <section className="final-support">
               <div className="visible-final">
<div className="wrapper-final">
    <div className="left-final">
        <img src="/images/start_screen/flag_german.png" alt="flag" className={"flag-final " + flags[0]}/>
        <img src="/images/start_screen/flag_estonian.png" alt="flag" className={"flag-final " + flags[1]}/>
    </div>
    <div className="right-final">
        <span className="level">
        {this.props.level}
        </span>
    </div>
</div>
               </div>
           </section>
        )
    }
}

