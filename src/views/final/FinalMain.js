import React, {Component} from "react";
import '../../css/final/final_main.css';
import OneButton from "../elements/OneButton";
import {getResponse} from "../../services/GetService";
import {StringsContext} from "../../i18";

export default class FinalMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            profile: {start: 'HI'}
        };
        this.setState = this.setState.bind(this);
    }

    async componentDidMount() {
        this.setState({profile: await getResponse('profile?lang=' + localStorage.getItem("lang")
                + "&level=" + localStorage.getItem("level"))});
    }

    getMonthOfDate(date) {
        let dt = new Date(date).getUTCDate();
        return dt < 10 ? '0' + dt : dt;
    }
    render() {
        let indNumber = this.state.profile.success;
        let indicator = indNumber + "%";
        return (
            <div className="final-main-wrapper">
            <div className="final-main">
<div className="internal">
<div className="final-main-left">
    <div className="red-square">
<div className="internal-square">
    <div className="calendar-line">
<div className="calendar-start">
    <img src="/images/final/start-calendar.png" alt="start" className="calendar-img"/>
    <span className="date date-start">{this.getMonthOfDate(this.state.profile.start)}</span>
</div>
        <div className="calendar-end">
            <img src="/images/final/end-calendar.png" alt="end" className="calendar-img"/>
            <span className="date date-end"> {this.getMonthOfDate(this.state.profile.end)}</span>
        </div>
    </div>
    <div className="text-line">
        {this.state.profile.texts + ' '}
        <span><StringsContext.Consumer>
            {value => value.texts}
        </StringsContext.Consumer></span>
    </div>
    <div className="indicator-line">
<div className="indicator-wrapper">
    <div className="indicator" style={{width: indicator}}></div>
</div>
        <div className="indicator-name"> {this.state.profile.success}%</div>
    </div>

</div>
    </div>
</div>
    <div className="final-main-right">
        <img src="/images/final/final-man.jpg" alt="professor" className="right-img"/>
    </div>
</div>
            </div>
                <div className="button-container">
                <OneButton title={'finish'}/>
                </div>
            </div>
        )}}