import React, {Component} from "react";
import '../../css/landing/landing_left.css';
import {StringsContext} from "../../i18";

export default class LandingPageLeft extends Component {
    render() {
        return (
            <section className="landing-left">
                <div className="levels">
                    <div className="level-1">A2</div>
                    <div className="level-1">B1</div>
                </div>
                <div className="description">
                    <h3 className="descr-text">
                        <StringsContext.Consumer>
                            {value => value.description}
                        </StringsContext.Consumer>
                    </h3>
                </div>
            </section>
        )}}