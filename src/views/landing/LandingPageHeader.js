import React, {Component} from "react";
import '../../css/landing/landing_header.css';
import {getStrings} from "../../i18";

export default class LandingPageHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            i18: getStrings(),
            langIndex: this.getInd(),
            langClasses: this.initialize(this.getInd())
        };
    }

    getInd = () => {
        if (localStorage.getItem('loc') === 'de') {
            return 1;
        }
        if (localStorage.getItem('loc') === 'ee') {
            return 2;
        }
        return 0;
    }

    initialize = (k) => {
        let initial = ['lng-back remove', 'lng-back remove', 'lng-back remove'];
        initial[k] = 'lng-back';
        return initial;
    }


    render() {
        const locs = ['en', 'de', 'ee'];
        const handleLangClick = (i) => {
            if (i !== this.state.langIndex) {
                localStorage.setItem('loc', locs[i]);
                let tempLangs = this.state.langClasses;
                let temp = tempLangs[i];
                tempLangs[i] = tempLangs[this.state.langIndex];
                tempLangs[this.state.langIndex] = temp;
                this.setState({langClasses: tempLangs, langIndex: i, i18: getStrings()});
            }
        };
        return (
            <section className="landing-header">
                <div className="elem-1">
                    <img src="/images/landing_screen/logo-large.png" alt="logo" className="logo"/>
                </div>
                <div className="elem-2">
                    <h1 className="header-text">{this.state.i18.startHeader}</h1>
                </div>
                <div className="elem-3">
                    <div className="flag">
                        <img src="/images/start_screen/flag_german.png" alt="flag" className="flag-img-1"/>
                    </div>
                    <div className="flag">
                        <img src="/images/start_screen/flag_estonian.png" alt="flag" className="flag-img-2"/>
                    </div>
                </div>
                <div className="elem-4">
                    <a href="#" className="lang-1-ref" onClick={() => handleLangClick(0, this.props.func('en'))}>
                    <h2 className="lang-1">
                        en
                    </h2>
                        <img src="/images/landing_screen/lang-button.png" alt="back" className={this.state.langClasses[0]}/>
                    </a>
                    <a href="#" className="lang-2-ref" onClick={() => handleLangClick(1, this.props.func('de'))}>
                    <h2 className="lang-2">
                        de
                    </h2>
                        <img src="/images/landing_screen/lang-button.png" alt="back" className={this.state.langClasses[1]}/>
                    </a>
                    <a href="#" className="lang-3-ref" onClick={() => handleLangClick(2, this.props.func('ee'))}>
                    <h2 className="lang-3">
                        et
                    </h2>
                        <img src="/images/landing_screen/lang-button.png" alt="back" className={this.state.langClasses[2]}/>
                    </a>
                </div>
            </section>
        )}}