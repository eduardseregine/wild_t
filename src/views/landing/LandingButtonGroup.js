import React, {Component} from "react";
import '../../css/landing/landing_button.css';
import {StringsContext} from "../../i18";
import {signInWithGoogle} from "../../services/FirebaseService";
import {getAuth, getName} from "../../services/UserService";

export default class LandingButtonGroup extends Component {
    render() {
        const handleClick = async () => {
            let name = await signInWithGoogle();
            if (getAuth().length > 0) {
                this.props.navigation("/start");
                this.props.chgeName(name);
            }
        }
        return (
            <section className="landing-button-section">
                <a href="#" onClick={handleClick} className="button-group">
                    <h1 className="button-text">
                        <StringsContext.Consumer>
                        {value => value.signUp}
                        </StringsContext.Consumer>
                    </h1>
                    <img src="/images/input_screen/next_button.png" alt="button" className="button"/>
                </a>
            </section>
        )}}