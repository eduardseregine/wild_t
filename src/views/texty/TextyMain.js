import React, {Component} from "react";
import Counter from "../elements/Counter";
import Buttons from "../elements/Buttons";
import TextField from "./TextField";
import {getText} from "../../services/TextService";
import '../../css/texty/texty_main.css';

export default class TextyMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addFunc: this.props.func,
            isNotLoaded: true,
            textObj: {},
            isTranslation: false
        }
        this.handleEngClick.bind(this);

    }

    async componentDidMount() {
        await this.getUpdatedText();
    }

    async getUpdatedText() {
        this.setState({isNotLoaded: false, textObj: await getText(this.handleClick)});
    }

    skip() {
        this.getUpdatedText().then({
        });
        this.props.funcClear();
    }

    handleClick = (word) => {
        this.setState({
            addFunc: this.props.func(word),
        })
    }

    getWordsCount() {
        if (this.state.textObj === undefined || this.state.textObj.lines === undefined) {
            return 0;
        }
        const mainText = this.state.textObj.lines;
        const count = mainText.filter((elem)=>elem.key > 0).length - 1;
        localStorage.setItem("count", count + "");
        return count;
    }

    handleEngClick = () => {
        this.setState({isTranslation: !this.state.isTranslation});
    }

    render() {
        let text = this.state.isTranslation ? this.state.textObj.translation : this.state.textObj.lines;
        let buttonsState = this.state.isTranslation ? ['invisible', ''] : ['', 'invisible'];
        let textClass = this.state.isTranslation ? 'translation' : '';
        return (
            <section className="main">
                <div className="form">
                    <TextField func={this.props.func}
                               mainText={text} styling={textClass}/>
                    <div className="down-menu">
                        <a href="#" className="eng-ref" onClick={this.handleEngClick}>
                            <img src="/images/texty/eng_button.png" alt="eng-button" className={"eng-img " + buttonsState[0]}/>
                            <img src="/images/texty/eng_pressed.png" alt="eng-pressed" className={"eng-img " + buttonsState[1]}/>
                        </a>
                        <Counter number={this.getWordsCount()}/>
                    </div>
                    <Buttons nextRef={'/input'} skipRef={'#'} onSkip={()=>this.skip()}/>
                </div>
            </section>
        )
    }
}