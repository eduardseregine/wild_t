import React, {Component} from 'react';
import {StringsContext} from "../../i18";

export default class TextyTextSupport extends Component {

    constructor() {
        super();
        this.state = {
            words: ['', '', ''],
        };
        this.remove = this.remove.bind(this);
        this.add = this.add.bind(this);
    }

    componentDidMount() {
        const words = [localStorage.getItem("first"),localStorage.getItem("second"),localStorage.getItem("third")];
        this.setState({words: words});
    }

    remainingItems = (w) => {
        return this.state.words.map((item) => item === w ? '' : item);
    }

    remove = (word) => {
        this.setState({
            words: this.remainingItems(word)
        });
        this.save();
    }

    clear = () => {
        this.setState({
            words: ['', '', '']
        });
        this.save();
    }

    add = (word) => {
        if (word.length > 3
            && this.state.words.filter((w) => w.length === 0).length > 0
            && this.state.words.filter((w) => w === word).length === 0) {
            let wrds = this.state.words;
            wrds[wrds.indexOf('')] = word;
            this.setState({words: wrds});
            this.save();
        }
    }

    save() {
        localStorage.setItem("first", this.state.words[0]);
        localStorage.setItem("second", this.state.words[1]);
        localStorage.setItem("third", this.state.words[2]);
    }

    render() {
        const getWordAlt = this.state.words.map((word) =>
            word.length === 0 ?
                <li className="word not-selected">
                    <a href="#" className="not-selected">
                        <StringsContext.Consumer>
                            {value => value.notSelected}
                        </StringsContext.Consumer>
                    </a>
                </li>
                : <li className="word">
                    <a href="#" onClick={() => this.remove(word)}>
                        <span className="remove">x</span>
                        <span className="word-text">{word}</span>
                    </a>
                </li>
        );
        return (
            <section className="support-words">
                <div className="visible">
                    <div className="words-wrapper">
                        <ul className="words">
                            {getWordAlt}
                        </ul>
                    </div>
                </div>
            </section>
        )
    }
}

