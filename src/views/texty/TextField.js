import React, {Component} from "react";
import '../../css/texty/texty_field.css';

export default class TextField extends Component {
    render() {
        return (
            <div className="input-field">
                <div className="input-block">
                    <div className="input-text">
                            <div className={"text-form " + this.props.styling}
                            >
                                "
                                {this.props.mainText}
                                <span> </span>
                                "
                            </div>
                    </div>
                </div>
            </div>
        )
    }
}