import React, {Component} from 'react';
import InputTextHeader from "./input/InputTextHeader";
import InputTextSupport from "./input/InputTextSupport";
import InputTextMain from "./input/InputTextMain";

export default class InputScreen extends Component {

    backHandling() {
        console.log("attemptCount");
        localStorage.setItem('attempt', localStorage.getItem('attempt') + "1");
    }
    render() {
        return (
            <div className="input-screen">
                <InputTextHeader header={'input'} backRef={'/texty'} onBack={()=>this.backHandling()}/>
                <InputTextSupport/>
                <InputTextMain/>
            </div>
        )
    }
}