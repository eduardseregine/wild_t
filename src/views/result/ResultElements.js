import React,{Component} from "react";
import '../../css/result_screen/result_elements.css';
import {StringsContext} from "../../i18";
import {postResponse} from "../../services/PostService";
import {InputContext} from "../../services/InputService";

export default class ResultElements extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            input: 'test',
            results: {'general': 0, 'understanding': 0, 'words': 0, 'attempts': 0, 'text': 0}
        };
        this.setState = this.setState.bind(this);
    }

    async componentDidMount() {
        const inputData = {input: localStorage.getItem("input"), lang: localStorage.getItem("lang"),
            level: localStorage.getItem("level"), attempts: localStorage.getItem('attempt')};
        this.setState({results: await postResponse('results', inputData), isLoaded:true});
        localStorage.setItem('attempt', "");
    }

    render() {
        let loading = !this.state.isLoaded ? () => {return (<p className="loading">
            <StringsContext.Consumer>
                {value => value.moment}
            </StringsContext.Consumer>
        </p>)} : () => {return (<span></span>)}
        return (
            <div className="result-elements">
                <div className="results-row">
                    <div className="results-col-1">
                        {loading()}
                        <div className="result-1">
                            <div className="result-text-first">
                                <p className="text">
                                    <StringsContext.Consumer>
                                        {value => value.total}
                                    </StringsContext.Consumer>
                                </p>
                            </div>
                            <div className="result-value-first">
                                <p className="text">{this.state.results.general}%</p>
                            </div>
                        </div>
                        <div className="result-2">
                            <div className="result-text-second">
                                <p className="text">
                                    <StringsContext.Consumer>
                                        {value => value.words}
                                    </StringsContext.Consumer>
                                </p>
                            </div>
                            <div className="result-value">
                                <p className="text">{this.state.results.words}%</p>
                            </div>
                        </div>
                    </div>
                    <div className="results-col-2">
                        <div className="central-image">
                            <img src="/images/result_screen/success-image.png" alt="success" className="success-image"/>
                        </div>
                    </div>

                    <div className="results-col-1">
                        <div className="result-3">
                            <div className="result-text">
                                <p className="text">
                                    <StringsContext.Consumer>
                                        {value => value.understanding}
                                    </StringsContext.Consumer>
                                </p>
                            </div>
                            <div className="result-value">
                                <p className="text">{this.state.results.understanding}%</p>
                            </div>
                        </div>
                        <div className="result-4">
                            <div className="result-text">
                                <p className="text">
                                    <StringsContext.Consumer>
                                        {value => value.attempts}
                                    </StringsContext.Consumer>
                                </p>
                            </div>
                            <div className="result-value-qty">
                                <p className="text">{this.state.results.attempts}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="text-block">
                    <div className="text-number-wrapper">
                        <div className="text-number-placeholder">
                            <p className="text-text">
                                <StringsContext.Consumer>
                                    {value => value.text}
                                </StringsContext.Consumer>
                                #</p>
                        </div>
                        <div className="text-number-number">
                            <p className="text-number">{this.state.results.textId}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}