import React, {Component} from 'react';
import AvatarGroup from "../AvatarGroup";
import {StringsContext} from "../../i18";

export default class ResultHeader extends Component {
    render() {
        let headerTitle = this.props.title === undefined ? () => {return (<StringsContext.Consumer>
            {value => value.congrats}
        </StringsContext.Consumer>)} : () => {return (<StringsContext.Consumer>
            {value => value.appraisal}
        </StringsContext.Consumer>)};
        return (
            <header className="header">
                <nav className="nav">
                    <div className="layer-wrapper">
                        <div className="second-layer">
                        </div>
                    </div>
                    <div className="items-absolute-1">
                        <div className="menu-items">
                            <div className="menu-item-1">
                                <a href="/start" className="home">
                                    <img className="home-img" src="/images/input_screen/home.png" alt="home"/>
                                </a>
                            </div>
                            <div className="menu-item-2">
                                <h1 className="title">
                                    {headerTitle()}</h1>
                            </div>
                            <div className="menu-item-3">

                            </div>
                        </div>
                    </div>
                    <div className="items-absolute-2">
                        <AvatarGroup/>
                    </div>
                </nav>
            </header>
        )
    }
}

