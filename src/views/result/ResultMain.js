import React, {Component} from 'react';
import OneButton from "../elements/OneButton";
import ResultElements from "./ResultElements";

export default class ResultMain extends Component {
    render() {
        return (
            <section className="main">
                <div className="form">
                    <div className="down-menu-result">
                        <ResultElements/>
                    </div>
                    <OneButton/>
                </div>
            </section>
        )
    }
}

