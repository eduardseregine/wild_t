import React, {Component} from "react";
import ResultHeader from "./result/ResultHeader";
import FinalMain from "./final/FinalMain";
import FinalSupport from "./final/FinalSupport";

export default class FinalScreen extends Component {

    render() {
        return (
            <section className="final-screen">
                <ResultHeader title={'appraisal'}/>
                <FinalSupport level={localStorage.getItem("level")} flag={localStorage.getItem("lang")}/>
                <FinalMain/>
            </section>
        )}}