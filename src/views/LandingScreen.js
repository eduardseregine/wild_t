import React, {Component} from "react";
import '../css/landing/landing_screen.css';
import LandingPageHeader from "./landing/LandingPageHeader";
import LandingPageLeft from "./landing/LandingPageLeft";
import LandingButtonGroup from "./landing/LandingButtonGroup";


export default class LandingScreen extends Component {

    render() {
        return (
            <section className="landing">
                <LandingPageHeader func={this.props.func}/>
                <div className="landing-row">
                    <div className="landing-column-1">
                        <div className="left-side-group">
                            <LandingPageLeft/>
                        </div>
                    </div>
                    <div className="landing-column-2">
                        <div className="button-group-right">
                            <LandingButtonGroup navigation={this.props.navigation} chgeName={this.props.chgName}/>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}