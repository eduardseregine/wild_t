import React, {Component, createRef} from "react";
import StartPageHeader from "./start/StartPageHeader";
import StartPageLeft from "./start/StartPageLeft";
import StartPageRight from "./start/StartPageRight";
import '../css/start_screen/start_screen.css';
import {clearSupport} from "../services/InputService";

export default class StartScreen extends Component {

    render() {
        clearSupport();
        return (
            <div className="start-page">
                <StartPageHeader name={this.props.name}/>
                <div className="start-row">
                    <div className="start-column-1">
                        <div className="right-group-left">
                            <StartPageRight navigation={this.props.navigation}/>
                        </div>
                        <StartPageLeft/>
                    </div>
                    <div className="start-column-2">
                        <div className="right-group-right">
                            <StartPageRight navigation={this.props.navigation}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}