import React, {Component} from "react";
import AvatarGroup from "../AvatarGroup";
import '../../css/start_screen/start_page_header.css';
import {StringsContext} from "../../i18";

export default class StartPageHeader extends Component {
    render() {
        return (
            <header>
            <div className="layer-wrapper">
                <div className="left-group">
                    <div className="logo-wrapper">
                        <img src="/images/start_screen/logo_small.png" alt="logo" className="logo-img"/>
                    </div>
                    <div className="start-text-wrapper">
                        <h1 className="start-text">
                            <StringsContext.Consumer>
                                {value => value.startHeader}
                            </StringsContext.Consumer>
                        </h1>
                    </div>
                </div>
                <div className="start-avatar-header">
                    <AvatarGroup/>
                </div>
            </div>
            </header>
        )
    }
}