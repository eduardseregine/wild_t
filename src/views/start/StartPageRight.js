import React, {Component} from "react";
import '../../css/start_screen/start_page_right.css';
import {StringsContext} from "../../i18";

export default class StartPageRight extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    async handleClick() {
        localStorage.setItem('attempt', "");
        this.props.navigation("/texty");
    }

    render() {

        return (
            <div className="start-block">
                <div className="start-control-group">
                    <a href="#" className="start-control-group-ref" onClick={this.handleClick}>
                    <h1 className="begin">
                        <StringsContext.Consumer>
                            {value => value.begin}
                        </StringsContext.Consumer>
                    </h1>
                        <img src="/images/input_screen/next_button.png" alt="button"
                             className="begin-button"/>
                    </a>
                </div>
            </div>
        )}}