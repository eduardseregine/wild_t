import React, {Component} from "react";
import RadioTicked from "../radio/RadioTicked";
import RadioUnticked from "../radio/RadioUnticked";
import '../../css/start_screen/start_page_left.css';
import StartPageRight from "./StartPageRight";

export default class StartPageLeft extends Component {

    books = ["Der Fluch der Mumie R.Böttcher",
        "Drei Kameraden von Erich Maria Remarque",
        "Margus, kassi- ja õunamoos Lea Kreinin",
        "Kuidas mu isa endale uue naise sai Reeli Reinaus"]
    constructor(props) {
        super(props);
        this.state = {
            flagToggle: ['no-display', ''],
            levelToggle: ['no-display', ''],
            book: this.books[2]
        };
    }
Y
    componentDidMount() {
        localStorage.setItem("level", "A2");
        localStorage.setItem("lang", "EE")
    }

    toBookOnFlag = () => {
        if (this.state.flagToggle[0].length === 0) {
            return this.state.levelToggle[0].length === 0 ?
                this.books[3] : this.books[2];
        }
        return this.state.levelToggle[0].length === 0 ?
            this.books[1] : this.books[0];

    }
    toBookOnLevel = () => {
        if (this.state.flagToggle[0].length === 0) {
            return this.state.levelToggle[0].length === 0 ?
                this.books[0] : this.books[1];
        }
        return this.state.levelToggle[0].length === 0 ?
            this.books[2] : this.books[3];
    }

    render() {
        const handleLevelClick = () => {
            this.setState({levelToggle: [this.state.levelToggle[1], this.state.levelToggle[0]]
            , book: this.toBookOnLevel()});
            this.state.levelToggle[0].length === 0 ? localStorage.setItem("level", "A2") : localStorage.setItem("level", "B1");
            console.log(localStorage.getItem("level"));
        };
        const handleFlagClick = () => {
            this.setState({flagToggle: [this.state.flagToggle[1], this.state.flagToggle[0]]
                , book: this.toBookOnFlag()});
            this.state.flagToggle[0].length === 0 ? localStorage.setItem("lang", "EE") : localStorage.setItem("lang", "DE");
            console.log(localStorage.getItem("lang"));
        };
        return (
            <div className="start-left">
                <div className="from-right">
                    <StartPageRight/>
                </div>
                {/*------------*/}
                <section className="selector section-1">
                    <div className="selector-1">
                        <div className={this.state.flagToggle[0]}>
                            <RadioTicked/>
                        </div>
                        <div className={this.state.flagToggle[1]}>
                            <a onClick={() => handleFlagClick()}>
                                <RadioUnticked/>
                            </a>
                        </div>

                        <div className="lang-flag-1">
                            <img src="/images/start_screen/flag_german.png" alt="flag" className="flag"/>
                        </div>
                    </div>
                    <div className="space"></div>
                    <div className="selector-2">
                        <div className={this.state.flagToggle[1]}>
                            <RadioTicked/>
                        </div>
                        <div className={this.state.flagToggle[0]}>
                            <a onClick={() => handleFlagClick()}>
                                <RadioUnticked/>
                            </a>
                        </div>
                        <div className="lang-flag-2">
                            <img src="/images/start_screen/flag_estonian.png" alt="flag" className="flag"/>
                        </div>
                    </div>
                </section>
                {/*------------*/}
                <section className="selector section-2">
                    <div className="selector-1">
                        <div className={this.state.levelToggle[0]}>
                            <RadioTicked/>
                        </div>
                        <div className={this.state.levelToggle[1]}>
                            <a onClick={() => handleLevelClick()}>
                                <RadioUnticked/>
                            </a>
                        </div>
                        <div className="level-1">
                            <h1 className="level-text">B1</h1>
                        </div>
                    </div>
                    <div className="space"></div>
                    <div className="selector-2">
                        <div className={this.state.levelToggle[1]}>
                            <RadioTicked/>
                        </div>
                        <div className={this.state.levelToggle[0]}>
                            <a onClick={() => handleLevelClick()}>
                                <RadioUnticked/>
                            </a>
                        </div>
                        <div className="level-2">
                            <h1 className="level-text">A2</h1>
                        </div>
                    </div>
                </section>
                {/*------------*/}
                <section className="book-section">
                    <div className="title">
                        <h1 className="book-title">
                            {this.state.book}
                        </h1>
                    </div>
                </section>
            </div>
        )
    }
}