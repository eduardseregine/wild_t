import React, {Component, createElement, createRef, useContext} from "react";
import InputTextHeader from "./input/InputTextHeader";
import TextyMain from "./texty/TextyMain";
import TextyTextSupport from "./texty/TextyTextSupport";
import OneButton from "./elements/OneButton";

export default class TextyScreen extends Component {

    constructor(props) {
        super(props);
        this.textSupportComponent = createRef();
    }
    render() {
        return (
            <div className="texty-screen">
                <InputTextHeader header={'texty'} backRef={'/start'}/>
                <TextyTextSupport ref={this.textSupportComponent}/>
                <TextyMain func={(w) => this.textSupportComponent.current.add(w)} funcClear={()=>this.textSupportComponent.current.clear()}/>
            </div>
        )}}