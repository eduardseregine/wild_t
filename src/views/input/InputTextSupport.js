import React, {Component} from 'react';
import {StringsContext} from "../../i18";

export default class InputTextSupport extends Component {
    render() {
        const words = [localStorage.getItem("first"),localStorage.getItem("second"),localStorage.getItem("third")];

        const getWordAlt = words.map((word) =>
            word !== null && word.length > 0 ?
                <li className="word-input">
                    <a href="#">
                        {word}
                    </a>
                </li> :
            <li className="word not-selected">
                <a href="#" className="not-selected">
                    <StringsContext.Consumer>
                        {value => value.notSelected}
                    </StringsContext.Consumer>
                </a>
            </li>
        );

        return (
           <section className="support-words">
               <div className="visible">
                   <div className="words-wrapper">
                   <ul className="words">
                           {getWordAlt}
                   </ul>
               </div>
               </div>
           </section>
        )
    }
}

