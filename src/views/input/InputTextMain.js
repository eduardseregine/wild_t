import React, {Component, createRef} from 'react';
import ExtraLetters from "../elements/ExtraLetters";
import Reset from "../elements/Reset";
import InputField from "../elements/InputField";
import Buttons from "../elements/Buttons";
import Counter from "../elements/Counter";
import {clearSupport} from "../../services/InputService";

export default class InputTextMain extends Component {

    constructor(props) {
        super(props);
        this.state = {
            num: 0,
        };
        this.inputComponent = createRef();
    }

    saveValue() {
        localStorage.setItem('input', this.inputComponent.current.getInput())
    }

    render() {
        return (
            <section className="main">
                <div className="form">
                    <Reset func = {() => {this.inputComponent.current.cancel()}}/>
                    <InputField wordsNumber = {(n) => {this.setState({num: n})}}
                    ref = {this.inputComponent}
                    />
                    <div className="down-menu">
                        <ExtraLetters func1 = {(k) => {this.inputComponent.current.addLetter(k)}}/>
                       <Counter number = {this.state.num} von={localStorage.getItem("count")}/>
                    </div>
                    <Buttons nextRef={'/result'} skipRef={'/texty'} funcGetValue={() => this.saveValue()} onSkip={()=>clearSupport()}/>
                </div>
            </section>
        )
    }
}

