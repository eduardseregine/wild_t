import React, {Component} from 'react';
import AvatarGroup from "../AvatarGroup";
import ArrowBack from "../elements/ArrowBack";
import {StringsContext} from "../../i18";

export default class InputTextHeader extends Component {
    render() {
        const header = this.props.header === 'input' ? [''].map(() =>
            <div><StringsContext.Consumer>
                {value => value.inputHeader}
            </StringsContext.Consumer></div>
        ) : [''].map(() =>
            <div><StringsContext.Consumer>
                {value => value.textyHeader}
            </StringsContext.Consumer></div>
        );
        return (
            <header className="header">
                <nav className="nav">
                    <div className="layer-wrapper">
                        <div className="second-layer">
                            <ArrowBack backRef={this.props.backRef} onBack={this.props.onBack}/>
                            <div className="arrow-end">
                            </div>
                        </div>
                    </div>
                    <div className="items-absolute-1">
                        <div className="menu-items">
                            <div className="menu-item-1">
                                <a href="/start" className="home">
                                    <img className="home-img" src="/images/input_screen/home.png" alt="home"/>
                                </a>
                            </div>
                            <div className="menu-item-2">
                                <h1 className="title">
                                    {header}
                                </h1>
                            </div>
                            <div className="menu-item-3">

                            </div>
                        </div>
                    </div>
                    <div className="items-absolute-2">
                        <AvatarGroup/>
                    </div>
                </nav>
            </header>
        )
    }
}

