import React, {Component} from "react";
import '../../css/elements/one_button.css';
import {StringsContext} from "../../i18";

export default class OneButton extends Component {
    handleClick() {
        localStorage.setItem("attempts", "");
    }
    render() {
        let title = this.props.title === undefined ? () => {return (
            <StringsContext.Consumer>
                {value => value.ok}
            </StringsContext.Consumer>
        )} : () => {return (
            <StringsContext.Consumer>
                {value => value.finish}
            </StringsContext.Consumer>
        )}
        let ref = this.props.title === undefined ? "/texty" : "/";
        return (
            <div className="buttons-element-one">
                <div className="buttons-wrapper-one">
                    <div className="button-one">
                        <div className="button-elem">
                            <img src="/images/input_screen/next_button.png" alt="next" className="img-button"/>
                        </div>
                        <div className="next-text-btn">
                            <a href={ref} className="next-ref-btn" onClick={this.handleClick}>
                                <p>
                                    {title()}
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )}}