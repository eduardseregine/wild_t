import React, {Component} from "react";
import {StringsContext} from "../../i18";

export default class Reset extends Component {
    render() {
        let handleClick = () => {
            this.props.func();
        }
        return (
            <div className="absagen">
                <div className="on-top-form">
                    <p>
                        <StringsContext.Consumer>
                            {value => value.cancel}
                        </StringsContext.Consumer>
                    </p>
                    <a href="#" className="bin-ref"
                    onClick={handleClick}>
                        <img src="/images/input_screen/delete_button.png" alt="bin" className="bin"/>
                    </a>
                </div>
            </div>
        )}}