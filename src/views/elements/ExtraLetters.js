import React, {Component} from "react";

export default class ExtraLetters extends Component {
    germanLetters = ['Ä', 'Ö', 'Ü', 'ß'];
    estonianLetters = ['Ä', 'Ö', 'Ü', 'Õ'];
    getLetters() {
        let lang = localStorage.getItem("lang");
        return lang === 'EE' ? this.estonianLetters : this.germanLetters;
    }
    render() {
        let letters = this.getLetters();
        let handleClick = (l) => {
            this.props.func1(l);
        }
        return (
            <div className="extra-letters">
                <ul className="chars">
                    <li className="char-1">
                        <a href="#" className="char-ref-1"
                        onClick={() => handleClick(letters[0])}>
                            {letters[0]}</a></li>
                    <li className="char-1">
                        <a href="#" className="char-ref-2"
                           onClick={() => handleClick(letters[1])}>
                            {letters[1]}</a></li>
                    <li className="char-1">
                        <a href="#" className="char-ref-3"
                           onClick={() => handleClick(letters[2])}>
                            {letters[2]}</a></li>
                    <li className="char-1">
                        <a href="#" className="char-ref-4"
                           onClick={() => handleClick(letters[3])}>
                            {letters[3]}</a></li>
                </ul>
            </div>
        )}}