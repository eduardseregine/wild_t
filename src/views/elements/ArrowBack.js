import React, {Component} from "react";

export default class ArrowBack extends Component {
    render() {
        return (
                        <div className="arrow-block">
                            <a href={this.props.backRef} className="arrow" onClick={() => this.props.onBack()}>
                                <img className="arrow-img" src="/images/input_screen/back_arrow.png" alt="arrow"/>
                            </a>
                        </div>
        )
    }
}