import React, {Component} from "react";
import '../../css/elements/buttons.css';
import {StringsContext} from "../../i18";

export default class Buttons extends Component {
    render() {
        return (
            <div className="buttons-element">
                <div className="buttons-wrapper">
                    <div className="buttons">
                        <div className="button-1">
                            <img src="/images/input_screen/skip_button.png" alt="skip" className="img-button-1"/>
                        </div>
                        <div className="button-2">
                            <img src="/images/input_screen/next_button.png" alt="next" className="img-button-2"/>
                        </div>
                        <div className="skip-text">
                            <a href={this.props.skipRef} className="skip-ref" onClick={this.props.onSkip}>
                                <p>
                                    <StringsContext.Consumer>
                                        {value => value.skip}
                                    </StringsContext.Consumer>
                                    </p>
                            </a>
                        </div>
                        <div className="next-text">
                            <a href={this.props.nextRef} className="next-ref" onClick={this.props.funcGetValue}>
                                <p>
                                    <StringsContext.Consumer>
                                        {value => value.verify}
                                    </StringsContext.Consumer>
                                    </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )}}