import React, {Component, useRef} from "react";

export default class InputField extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lastIndex: 0,
            textAreaValue: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
    }

    componentDidMount() {
        let lastInput = '';
        if (localStorage.getItem('attempt').length > 0) {
            lastInput = localStorage.getItem("lastInput");
        } else {
            localStorage.setItem("lastInput", "");
        }
        this.setState({textAreaValue: lastInput});
        this.nameInput.focus();
        this.countWords(lastInput);
    }

    handleChange(event) {
        let text = event.target.value;
        this.setState({
            textAreaValue: text,
            lastIndex: event.target.selectionEnd
        });
        localStorage.setItem("lastInput", text);
        this.countWords(text);
    }

    countWords(text) {
        if (text.length > 0) {
            let wordsCount = text.split(' ').length;
            wordsCount = text[text.length - 1] === ' ' ? wordsCount - 1 : wordsCount;
            this.props.wordsNumber(wordsCount);
        }
    }

    handleKeyPress(event) {
        if (event.keyCode === 8 && this.state.textAreaValue.length < 2) {
            this.props.wordsNumber(0);
        }
    }

    handleFocus(event) {
        this.setState({lastIndex: event.target.selectionEnd});
    }

    addLetter(letter) {
        let text = this.state.textAreaValue;
        let l = letter;
        if (text.length !== 0 && text.at(this.state.lastIndex - 1) !== '.'
            && text.at(this.state.lastIndex - 2) !== '.') {
            l = letter.toLowerCase();
        }
        if (this.state.lastIndex >= text.length) {
            text = text + l;
        } else {
            text = text.slice(0, this.state.lastIndex) + l +
                text.slice(this.state.lastIndex, text.length);
        }
        this.countWords(text);
        this.setState({textAreaValue: text});
        this.nameInput.focus();
        localStorage.setItem("lastInput", text);
    }

    cancel() {
        this.setState({textAreaValue: ''});
        this.props.wordsNumber(0);
        localStorage.setItem("lastInput", "");
        this.nameInput.focus();
    }

    getInput() {
        let res = '';
        if (this.state.textAreaValue !== null && this.state.textAreaValue !== undefined) {
            res = this.state.textAreaValue;
        }
        return res;
    }

    render() {
        return (
            <div className="input-field">
                <div className="input-block">
                    <div className="input-text">
                            <textarea id="text" name="text" rows="2" maxLength="400"
                                      value={this.state.textAreaValue}
                                      ref={(textarea) => { this.nameInput = textarea; }}
                                      placeholder="Licht is" className="input-form"
                                      onClick={this.handleFocus}
                                      onKeyDown={this.handleKeyPress}
                                      onChange={this.handleChange}
                            />
                    </div>
                </div>
            </div>
        )
    }
}