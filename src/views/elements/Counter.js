import React, {Component} from "react";
import '../../css/elements/counter.css';
import {StringsContext} from "../../i18";

export default class Counter extends Component {
    render() {
        let von = () => {return (<StringsContext.Consumer>
            {value => value.words}
        </StringsContext.Consumer>)};
        let words = '';
        if (this.props.von !== undefined && this.props.von !== "0" && this.props.von.length > 0) {
            von = () => {return (<StringsContext.Consumer>
                {value => value.wordsVon}
            </StringsContext.Consumer>)};
            words = this.props.von;
        }
        return (
            <div className="counter-element">
                <div className="counter">
                    <ul className="counter-elements">
                        <li className="counter-elem-1"><p>
                            <span className="digit-1">{(this.props.number - this.props.number % 10) / 10}</span>
                            <span className="digit-2"></span>
                            <span className="digit-3">{this.props.number % 10}</span>
                        </p></li>
                        <li className="counter-elem-2">
                            <p className="counter-text"> {von()}</p>
                        </li>
                        <li className="counter-elem-3"><p>{words}</p></li>
                    </ul>
                </div>
            </div>
        )}}