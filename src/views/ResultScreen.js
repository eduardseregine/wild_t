import React, {Component} from 'react';
import ResultHeader from "./result/ResultHeader";
import ResultMain from "./result/ResultMain";
import {clearSupport} from "../services/InputService";
import FinalSupport from "./final/FinalSupport";

export default class ResultScreen extends Component {
    render() {
        clearSupport();
        return (
            <div className="result-screen">
                <ResultHeader/>
                <FinalSupport/>
                <ResultMain/>
            </div>
        )
    }
}