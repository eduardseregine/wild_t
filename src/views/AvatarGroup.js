import React, {Component} from "react";
import {getName, nameContext} from "../services/UserService";

export default class AvatarGroup extends Component {

    render() {
        return (
            <div className="items-absolute-2">
                <div className="avatar-group">
                    <a href="/final" className="avatar">
                        <img className="avatar-img"
                             src="/images/input_screen/avatar.png"
                             alt="avatar"/>
                        <h1 className="user-name">
                            <nameContext.Consumer>
                                {value => value}
                            </nameContext.Consumer>
                        </h1>
                    </a>
                </div>
            </div>
        )}
}