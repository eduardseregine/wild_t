import LocalizedStrings from "react-localization";
import {createContext} from "react";

export const strings = new LocalizedStrings({
    en:{
        description:"Learn by reading well-known texts and reproducing them in writting",
        startHeader: "We learn German by texting",
        signUp: "Sign up",
        begin: "begin",
        skip: "skip",
        verify: "verify",
        inputHeader: "Produce the text",
        cancel: "Cancel",
        notSelected: "Not selected",
        congrats: "Congratulations",
        ok: 'Ok',
        textyHeader: "Read and remember the text",
        total: "Total",
        words: "Words",
        wordsVon: "Words of",
        understanding: "Sense",
        attempts: "Attempts",
        text: "Text",
        texts: "Texts",
        appraisal: "Appraisal",
        finish: "finish",
        moment: "One moment"
    },
    de: {
        description:"Lernen Sie, indem Sie bekannte Texte lesen und schriftlich wiedergeben",
        startHeader: "Wir learnen Deutsch by texten",
        signUp: "anmelden",
        begin: "beginn",
        skip: "hüpfen",
        verify: "prüfen",
        inputHeader: "Anwenden den Text",
        cancel: "Absagen",
        notSelected: "Keine auswählen",
        congrats: "Glückwünsche",
        ok: 'OK',
        textyHeader: "Lesen den Text",
        total: "Gesamt",
        words: "Wörter",
        wordsVon: "Wörter von",
        understanding: "Verstand",
        attempts: "Versuchen",
        text: "Der Text",
        texts: "Texte",
        appraisal: "Bewertung",
        finish: "beenden",
        moment: "einen Moment",
    },
    ee: {
        description:"Õppige lugedes tuntud tekste ja neid kirjalikult taasesitades",
        startHeader: "eesti keelt õpime kirjutades",
        signUp: "Registreeri",
        begin: "alustada",
        skip: "hüpe",
        verify: "tuli",
        inputHeader: "Rakenda tekst",
        cancel: "tühistada",
        notSelected: "ei vali ühtegi",
        congrats: "Palju õnne",
        ok: 'Okei',
        textyHeader: "Loe ja jäta tekst pähe",
        total: "Kogu",
        words: "Sõnad",
        wordsVon: "Sõna st",
        understanding: "Mõistmine",
        attempts: "Katse",
        text: "Tekst",
        texts: "Tekstid",
        appraisal: "Hindamine",
        finish: "lõpp",
        moment: "oota"
    }
});

export const getStrings = () => {
    if (localStorage.getItem("loc") !== null) {
        strings.setLanguage(localStorage.getItem('loc'));
    } else {
        strings.setLanguage('en');
    }
    return strings;
}

export const StringsContext = createContext(getStrings());

export const lang = {
    l: 0
}