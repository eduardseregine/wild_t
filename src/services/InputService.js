import {createContext} from "react";

export const InputContext = createContext('');

export function clearSupport() {
    localStorage.setItem("first", "");
    localStorage.setItem("second", "");
    localStorage.setItem("third", "");
}