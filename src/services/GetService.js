import {getAuth} from "./UserService";

export const getResponse = async (url) => {
    let resp = {}
    let token = getAuth();
    await fetch('https://app-5422c57e-5bda-459e-b426-4a3e254f9830.cleverapps.io/' + url, {  // Enter your IP address here
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : token
        }})
        .then(res => res.json())
        .then(
            (result) => {
                resp = result;
                console.log(result);
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
            }
        );
    return resp;
}