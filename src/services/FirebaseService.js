import {auth, provider} from "./FirebaseConfig";
import {setUser} from "./UserService";

export const signInWithGoogle = async () => {
        let name = 'someValue';
        await auth
            .signInWithPopup(provider)
            .then((result) => {
                const credential = result.credential;
                console.log(credential);
                const user = result.user;
                setUser(user.displayName.split(" ")[0], user.uid, result.credential.idToken.split(".")[0]);
                name = user.displayName.split(" ")[0];
            }).catch((error) => {
            const email = error.email;
            const credential = error.credential;
            console.log(email, credential);
        });
        return name;
}