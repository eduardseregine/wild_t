import {getAuth} from "./UserService";

export const postResponse = async (url, jsonData) => {

    let dataRead = {}
    let token = getAuth();

    // Send data to the backend via POST
    await fetch('https://app-5422c57e-5bda-459e-b426-4a3e254f9830.cleverapps.io/' + url, {  // Enter your IP address here

        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : token
        },
        body: JSON.stringify(jsonData) // body data type must match "Content-Type" header
    }).then(function (response) {
       return response.json();
    })
        .then(function (data) {
                dataRead = data;
                console.log("receiving");
                console.log(data);
            },
            (error) => {
                return error;
            });
    return dataRead;
}
