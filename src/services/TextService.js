import {getResponse} from "./GetService";
import React from "react";


export async function getText(func) {
    let res = [];
    let obj = {};
    await loadUpdatedText().then((result) => {
        let arr = updateText(result.array, func);
        res.push(...arr);
        obj = {lines: res, translation: result.translation};
    });
    return obj
}

async function loadUpdatedText() {
    const array = [''];
        const resp = await getResponse('strings?attempt=' + localStorage.getItem("attempt").length +
        '&lang=' + localStorage.getItem("lang") + '&level=' + localStorage.getItem("level"));
        array.push(...resp.text);
    return {array: array, translation: resp.translation};
}

function updateText(array, func) {
    let key = 1;
    let keySeparator = -1;
    return array.map((text) =>
        text !== '.' && text !== ',' && text !== '!' && text !== '?' && text !== ':' && text !== ';' ?
            <span key={key++}> <span className={getClass(text)} onClick={() => func(text)}>
            {text}
                </span></span> :<span key={keySeparator--}><span className="space">
            {text}
                </span></span>
    );
}

const getClass = (text) => {return text.length > 3 ? 'main-text-word' : 'small-text-word'};