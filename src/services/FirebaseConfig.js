// Import the functions you need from the SDKs you need
import 'firebase/auth';
import React, {Component} from "react";
import firebase from "firebase/compat";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries



// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
   const firebaseConfig = {
        apiKey: "AIzaSyBZhBODohEdCI6h8m83kuzJ3xIVI289N0k",
        authDomain: "wildt-383c6.firebaseapp.com",
        projectId: "wildt-383c6",
        storageBucket: "wildt-383c6.appspot.com",
        messagingSenderId: "64739006511",
        appId: "1:64739006511:web:8a9e33e74b877a962eceda",
        measurementId: "G-2DYL8YJFL4"
    };

// Initialize Firebase

const firebaseApp = firebase.initializeApp(firebaseConfig);

export const auth = firebaseApp.auth();
export const storage = firebaseApp.storage();
export const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({
    'login_hint': 'user@example.com'
});
