import {createContext} from "react";

export const nameContext = createContext(getName());

export function setUser(name, uuid, token) {
    localStorage.setItem("name", name);
    localStorage.setItem("auth", token + "." + uuid);
}

export function getName() {
    let name = "";
    if (localStorage.getItem("name") !== null) {
        name = localStorage.getItem("name");
    }
    return name;
}

export function getAuth() {
    let auth = "";
    if (localStorage.getItem("auth") !== null) {
        auth = 'Bearer' + localStorage.getItem("auth");
    }
    return auth;
}