import './App.css';
import StartScreen from "./views/StartScreen";
import {Route, Routes, useNavigate} from "react-router-dom";
import LandingScreen from "./views/LandingScreen";
import InputScreen from "./views/InputScreen";
import React, {useState} from "react";
import {getStrings, StringsContext} from "./i18";
import ResultScreen from "./views/ResultScreen";
import TextyScreen from "./views/TextyScreen";
import {getName, nameContext} from "./services/UserService";
import {InputContext} from "./services/InputService";
import FinalScreen from "./views/FinalScreen";

function App() {
    const navigation = () => useNavigate() // extract navigation prop here

    const [string, setString] = useState({
        str: getStrings()
    });
    const [name, setName] = useState({
        nme: getName()
    })
    const changeLang = (loc) => {
        localStorage.setItem('loc', loc);
        setString({str: getStrings()});
    }
    const changeName = (n) => {
        if (n !== undefined && n.length > 0) {
            setName({nme: n});
        } else {
            setName({nme: getName()});
        }
    }
    return (
        <div>
            <nameContext.Provider value={name.nme}>
                <InputContext.Provider value={localStorage.getItem('input')}>
                    <StringsContext.Provider value={string.str}>
                        <Routes>
                            <Route path="/"
                                   element={<LandingScreen func={(loc) => changeLang(loc)} navigation={navigation()}
                                                           chgName={(n) => changeName(n)}/>}/>
                            <Route path="/start" element={<StartScreen navigation={navigation()}/>}/>
                            <Route path="/texty" element={<TextyScreen />}/>
                            <Route path="/input" element={<InputScreen />}/>
                            <Route path="/result" element={<ResultScreen />}/>
                            <Route path="/final" element={<FinalScreen />}/>
                        </Routes>
                    </StringsContext.Provider>
                </InputContext.Provider>
            </nameContext.Provider>
        </div>
    );
}

export default App;
